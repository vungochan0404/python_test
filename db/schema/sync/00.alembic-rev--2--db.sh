#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../../.." && pwd)

cd $AH
    PYTHONPATH=$AH  pipenv run  alembic upgrade head
