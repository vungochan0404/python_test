# WELCOME

Welcome to Tech JDI's Python Technical Test - Falcon and SQLAlchemy.

In the test, you will build upon an existing git repository using the Falcon REST API framework and SQLAlchemy ORM.

We will also be using Alembic for creating database migration scripts.

There is only 1 test scenario in this test. It is identified by **SCENARIO 1**.


# PRE-REQUISITES

Firstly, please fork or clone this repository into your own account. Upon completion, please create a Merge Request and tag @yusry and @namgivu . If using another git repository other than GitLab, please send the URL to the git repository to your contact at Tech JDI.

The instructions require `pyenv` to be preinstalled. We will also require `docker` and `docker-compose` commands to be avaialble if you want to utilise our database scripts.

## Installing Python 3.9.x

We will be using Python 3.9.x for the test. The current latest version as of 7th June 2021 is 3.9.5

### Get latest pip
```
pyenv local 3.9.5
python -m pip install --upgrade pip
```

### Require `pipenv` via `pip install`
```
python -m pip install pipenv
```

### Install package pre-listed in `Pipfile`
```
PIPENV_VENV_IN_PROJECT=1 python -m pipenv install
```

### Create .env file
Create `.env` for `localhost`

```
ENV=localhost ./_env/gen.sh
```

## Setting up PostgreSQL database

### Create blank database 
```
# You may need to install docker-compose before starting the database!

db/postgres/up.sh
db/schema/blank-db.sh
```

### Sync and migrate database
Please refer to [db/schema/_.md](db/schema/_.md)

## Starting API server
```
_bin/api/start.sh
```

## Testing the Mock API end-point
```
_bin/tests/run.sh
```

# References

--- API app via Falcon framework 

https://gitlab.com/namgivu/falcon-start

--- Database migration

https://gitlab.com/namgivu/alembic-start

--- Basic Alembic mechanism

https://github.com/tomlaszczuk/falcon-sqlalchemy-template


# SCENARIOS

## Scenario 1

In scenario 1, we will be testing your familiarity with Falcon, SQLAlchemy, and Alembic; these are the basic building blocks of our API stack.

We will be building an end-point called `/users`, which allows us to show all users in the system, and a related end-point `/users/1` that allows us to retrieve a specific user.

Please observe the following screenshot of a use-case of this end-point.

![](./zQUESTIONS.md/i/210604.profile.png)

If you have successfully started the template application, you can access the following mock endpoints that return the expected result of the two end-points you are expected to build.

### Expected vs Mock end-point URLs
```
target endpoint    mock endpoint
---------------    ---------------------- 
GET /users         /mocks?m=GET&n=users
GET /users/1       /mocks?m=GET&n=users/1
```

### Expected outcomes:

1. `GET /users` end-point is complete
2. `GET /users/{user_id}` end-point is complete
3. Well designed database scheme/SQL Alchemy models representation of the data structure required for the end-points. We will look at your models and resulting schema.
4. Alembic migration scripts.

Optionally,

5. A script to seed sample data that you have created as part of the development of this test.

There is no need for any diagrams to represent your data structure, nor are any POST (create) and PUT (update) end-points required.

# END OF TEST.
