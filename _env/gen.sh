#!/bin/bash
docstring='
Generate .env from .env0 at ./_env/$ENV/.env0.sh
Why we have .env0 instead of using .env directly? --> Cause we have envvar(s) created from other envvar eg DB_URL
eg
ENV=localhost ./_env/gen.sh
'
[ -z $ENV ] && (echo 'Envvar $ENV is required'; kill $$)

    SH=$(cd `dirname $BASH_SOURCE` && pwd)
    AH=$(cd "$SH/.." && pwd)
    cd $AH
        env0_f="$AH/_env/$ENV/.env0.sh" ; [ ! -f $env0_f ] && (echo "File not found at $env0_f" | grep $ENV --color=always; kill $$)
        env_f="$AH/.env"               ; [ -f $env_f ] && (echo "File already exists at $env_f"; kill $$)
            set -e  # halt if error ref. https://stackoverflow.com/a/3474556/248616
                source $env0_f
                source "$SH/apply_shell_expansion.sh" ; apply_shell_expansion $env0_f > $env_f
